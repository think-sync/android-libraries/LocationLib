package com.thinknsync.positionmanager;

import com.thinknsync.observerhost.ObserverHost;
import com.thinknsync.observerhost.TypedObserver;

/**
 * Created by shuaibrahman on 10/17/16.
 */

public interface GpsPositionManager extends ObserverHost<TrackingData> {
//    long minTime = 60 * 3 * 1000;;
    long minTime = 10000;
    float minDistance = 50;
    int minAcceptableAccuracy = 50;

    void getLastLocation(TypedObserver<TrackingData> locationObserver) throws SecurityException;
    long getMinTime();
    float getMinDistance();
    int getMinAcceptableAccuracy();
    void setMinTime(long minTime);
    void setMinDistance(float minDistance);
    void setMinAcceptableAccuracy(int minAcceptableAccuracy);
    void startTracking() throws SecurityException;
    void stopTracking();
}
