package com.thinknsync.positionmanager;

import android.app.Notification;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.thinknsync.androidnotification.AndroidNotification;
import com.thinknsync.androidnotification.NotificationObject;
import com.thinknsync.androidnotification.NotifyUser;
import com.thinknsync.logger.AndroidLogger;
import com.thinknsync.logger.Logger;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.objectwrappers.FrameworkWrapper;
import com.thinknsync.objectwrappers.InvalidWrapperArgumentException;
import com.thinknsync.objectwrappers.PendingIntentWrapper;
import com.thinknsync.observerhost.TypedObserver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class BaseLocationTrackerService extends Service implements GpsPositionManagerService {

    private GpsPositionManager positionManager;
    private NotificationObject notificationObject = new NotificationObject();

    protected AndroidContextWrapper contextWrapper;
    protected Logger logger;

    private final int SERVICE_ID = 1111;

    private IBinder localBinder = new LocalBinder();

    @Override
    public void onCreate() {
        super.onCreate();
        logger = AndroidLogger.getInstance(false);
        contextWrapper = new AndroidContextWrapper(this);
        this.positionManager = LocationTracker.getInstance(contextWrapper);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if(intent.getExtras() != null){
            try {
                notificationObject = new NotificationObject().fromJson(new JSONObject(intent
                        .getExtras().getString(NotificationObject.tag)));
            } catch (JSONException e) {
                e.printStackTrace();
                notificationObject = new NotificationObject();
            }
        }
        NotifyUser<Notification> androidNotification = new AndroidNotification(contextWrapper);
        androidNotification.init(notificationObject, null);

        startForeground(SERVICE_ID, androidNotification.getNotificationObject());
        startTracking();

        return START_NOT_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        return localBinder;
    }

    @Override
    public void onDestroy() {
        stopTracking();
        logger.debugLog("location manager service", "Service Stopped!");
        super.onDestroy();
    }

    @Override
    public void getLastLocation(TypedObserver<TrackingData> locationObserver) throws SecurityException {
        positionManager.getLastLocation(locationObserver);
    }

    @Override
    public long getMinTime() {
        return positionManager.getMinTime();
    }

    @Override
    public float getMinDistance() {
        return positionManager.getMinDistance();
    }

    @Override
    public int getMinAcceptableAccuracy() {
        return positionManager.getMinAcceptableAccuracy();
    }

    @Override
    public void setMinTime(long minTime) {
        positionManager.setMinTime(minTime);
    }

    @Override
    public void setMinDistance(float minDistance) {
        positionManager.setMinDistance(minDistance);
    }

    @Override
    public void setMinAcceptableAccuracy(int minAcceptableAccuracy) {
        positionManager.setMinAcceptableAccuracy(minAcceptableAccuracy);
    }

    @Override
    public void startTracking() throws SecurityException {
        positionManager.startTracking();
    }

    @Override
    public void stopTracking() {
        positionManager.stopTracking();
    }

    @Override
    public void validateObject(Object object) {
        try {
            if(!(object instanceof PendingIntentWrapper)){
                throw  new InvalidWrapperArgumentException();
            }
        } catch (InvalidWrapperArgumentException e){
            e.printStackTrace();
        }
    }

    @Override
    public void setupClickAction(FrameworkWrapper frameworkWrapper) {
        validateObject(frameworkWrapper);
    }

    @Override
    public void addToObservers(TypedObserver<TrackingData> observer) {
        positionManager.addToObservers(observer);
    }

    @Override
    public void addToObservers(List<TypedObserver<TrackingData>> observers) {
        positionManager.addToObservers(observers);
    }

    @Override
    public void removeObserver(TypedObserver<TrackingData> observer) {
        positionManager.removeObserver(observer);
    }

    @Override
    public void clearObservers() {
        positionManager.clearObservers();
    }

    @Override
    public void notifyObservers(TrackingData trackingData) {
        positionManager.notifyObservers(trackingData);
    }

    @Override
    public void notifyError(Exception e) {
        positionManager.notifyError(e);
    }

    public class LocalBinder extends Binder {
        public BaseLocationTrackerService getService() {
            return BaseLocationTrackerService.this;
        }
    }
}
