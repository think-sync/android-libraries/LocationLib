package com.thinknsync.positionmanager;

/**
 * Created by shuaib on 5/16/17.
 */

public interface ObjectValidator {
    void validateObject(Object object);
}
