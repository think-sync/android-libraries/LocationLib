package com.thinknsync.positionmanager;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;

import androidx.appcompat.app.AppCompatActivity;

import com.thinknsync.objectwrappers.AndroidActivityWrapper;

/**
 * Created by letsgoandroid on 8/13/17.
 */

public class LocationStateHandler implements LocationStateListener {
    private final AppCompatActivity activity;

    public LocationStateHandler(AndroidActivityWrapper activity) {
        this.activity = (AppCompatActivity) activity.getFrameworkObject();
    }


    @Override
    public boolean isLocationServicesOn() {
        LocationManager lm = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);
        boolean gps_enabled = false;

        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ignored) {
        }

        return gps_enabled;
    }

    @Override
    public void showLocationServicesDialog() {
        activity.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
    }
}
