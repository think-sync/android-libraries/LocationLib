package com.thinknsync.positionmanager;

import android.content.Context;
import android.location.Location;
import android.os.Looper;

import androidx.annotation.NonNull;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.thinknsync.objectwrappers.AndroidContextWrapper;
import com.thinknsync.observerhost.ObserverHostImpl;
import com.thinknsync.observerhost.TypedObserver;
import com.thinknsync.observerhost.TypedObserverImpl;

import java.util.List;

public class LocationTracker extends ObserverHostImpl<TrackingData> implements GpsPositionManager {

    private static GpsPositionManager positionManager;

    private long minTime = GpsPositionManager.minTime;
    private float minDistance = GpsPositionManager.minDistance;
    private int minAcceptableAccuracy = GpsPositionManager.minAcceptableAccuracy;

    private FusedLocationProviderClient fusedLocationClient;
    private LocationCallback locationCallback;

    private LocationTracker(Context context){
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
    }

    public static GpsPositionManager getInstance(AndroidContextWrapper contextWrapper){
        if(positionManager == null){
            positionManager = new LocationTracker(contextWrapper.getFrameworkObject());
        }
        return positionManager;
    }

    @Override
    public void getLastLocation(final TypedObserver<TrackingData> locationObserver) throws SecurityException {
        fusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null && location.getAccuracy() < minAcceptableAccuracy) {
                    TrackingData locationData = getTrackingDataFromLocation(location);
                    locationObserver.update(LocationTracker.this, locationData);
                } else {
                    getLastLocation(locationObserver);
                }
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                getLastLocation(locationObserver);
            }
        });
    }

    @Override
    public long getMinTime() {
        return minTime;
    }

    @Override
    public float getMinDistance() {
        return minDistance;
    }

    @Override
    public int getMinAcceptableAccuracy() {
        return minAcceptableAccuracy;
    }

    @Override
    public void setMinTime(long minTime) {
        this.minTime = minTime;
    }

    @Override
    public void setMinDistance(float minDistance) {
        this.minDistance = minDistance;
    }

    @Override
    public void setMinAcceptableAccuracy(int minAcceptableAccuracy) {
        this.minAcceptableAccuracy = minAcceptableAccuracy;
    }

    @Override
    public void startTracking() throws SecurityException {
        LocationRequest locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(getMinTime());
        locationRequest.setSmallestDisplacement(getMinDistance());

        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(LocationResult locationResult) {
                if (locationResult == null) {
                    return;
                }

                List<Location> locations = locationResult.getLocations();
                if(locations.size() > 0){
                    Location location = locations.get(locations.size() - 1);
                    if(location != null) {
                        TrackingData locationData = getTrackingDataFromLocation(location);
                        notifyObservers(locationData);
                    }
                }
            }
        };

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, Looper.myLooper());
    }

    private TrackingData getTrackingDataFromLocation(Location location){
        TrackingData locationData = new TrackingData();
        locationData.setLat(location.getLatitude());
        locationData.setLon(location.getLongitude());
        locationData.setAccuracy(location.getAccuracy());
        locationData.setTimeStamp(location.getTime());

        return locationData;
    }

    @Override
    public void stopTracking() {
        fusedLocationClient.removeLocationUpdates(locationCallback);
    }
}
