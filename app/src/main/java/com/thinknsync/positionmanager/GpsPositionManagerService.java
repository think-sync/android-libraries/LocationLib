package com.thinknsync.positionmanager;

import com.thinknsync.objectwrappers.FrameworkWrapper;

public interface GpsPositionManagerService extends GpsPositionManager, ObjectValidator {
    String key_min_time = "minTime";
    void setupClickAction(FrameworkWrapper frameworkWrapper);
}
